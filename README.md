## README

```yaml
Package: pheatmap
Title: Pretty Heatmaps
Version: 1.0.12.9000
Date: 2021-09-23
Authors@R (parsed):
    * Raivo Kolde <rkolde@gmail.com> [cre, aut]
    * pooranis [ctb] (<https://orcid.org/0000-0003-4371-7290>)
Description: Implementation of heatmaps that offers more control
    over dimensions and appearance. Fork of CRAN package
    <https://CRAN.R-project.org/package=pheatmap>.
License: GPL-2
URL: https://gitlab.com/pooranis/pheatmap
Depends:
    R (>= 2.0)
Imports:
    graphics,
    grDevices,
    grid,
    gtable,
    RColorBrewer,
    scales,
    stats
LazyLoad: yes
NeedsCompilation: no
RoxygenNote: 7.1.2
```
